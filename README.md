# Step 1: Determine exact ANKI export format Syntax

- "Notizen mit unformatiertem Text" / "HTML in Feldern zulasen"
- Unterschiedliche Felder werden durch Tab getrennt (\t)
    - Schlagwörter sind auch normales Feld (werden als letztes Feld angehängt)
    - Nach dem Letzten Feld kein Tab
- Wenn Felder leer gelassen werden, werden sie durch einen leeren String signalisiert ==> Multiple Tabs hintereinander möglich
- Keine Leerzeile am Ende der Datei
- Merkwürdig escapte Strings:
    - Sofern ein paar Anführungszeichen in einem Feld vorkommen...
        - Wird dieses Feld von Anführungszeichen umschlossen
        - Wird jedem Anführungszeichen im Feld noch ein Anführungszeichen vorangehängt
    - Das scheint beim Import ignoriert werden zu können

TestKarte
: Das hier ist eine Textrückseite

# Step 2: Write Tests to run the script on

# Step 2: Use awk to create a format similar to ANKI import format
- Preprocessing: Sichergehen, dass Überschriften von jeweils einer Leerzeile umgeben sind. Wenn nein, eine hinzufügen.
- Berücksichtigt werden nur Definition-Tags
- Mapping von Syntax auf AWK-Ersetzungen erstmal hardcoden


# Step 3: Verify 2+3 by manually doing the project-changes to a new project and then trying to import an old format


# Following are the tests:

# VL 6: Thema der Vorlesung

Das hier ist eine gewöhnliche **Definition**
: Das hier wird dann zur Rückseite der ANKI-Karte

## Subthema der Definition

Definition mit mehreren Textblöcken
: Diese Defintion besitzt mehrere Absätze
: Diese Absätze werden i.d.R. als stilistisches Mittel genutzt.
: Sie sollten einfach durch ein `<br>` ersetzt werden

### SubSubThema: Sonderzeichen blabla

Definition mit Aufzählung
: Diese Definition enthält eine Aufzählung
: * Item 1
  * Item 2
  * Item 3

Tadaa - noch eine Aufzählung
: * Item 1
  * Item 2

Diesmal eine Nummerierung
: 1. Element1
  2. Element2