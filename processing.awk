BEGIN {
    # Useful variables
    # =====================
    # FS: Input field separator (default " ")
    # RS: Input record separator (default "\n")

    # NR: number of the current record
    # NF: number of fields in the current record
    # ENVIRON["USER"]: Environment-variable $USER

    # OFS: output field separator (default " ")
    # ORS: output record separator (default = "\n")

    # , separates two fields from another (inserting the OFS)

    # For our use case
    # Field = Vorderseite / Rückseite / VL X
    FS = "\n"
    RS = "\n\n"
    OFS = "\t" # should be \t
    ORS = "\n" # should be \n

    s_header = "<div class=explanation>"
    s_footer = "</div>"

    lecture_number = "0"
}

# Register where Enumerations and Itemizations are
# Surround them with an Explanation-Tag.
# Every /\n: / marks its own section that can be explanation-like or simple-answer-like
/\n: / {

    front_page = $1

    for (i = 2; i <= NF; i++) { # forall lines in a block surrounded by a blank line
        # if $i matches
        #        an enumeration       or      an itemization
        if ($i ~ "^:?[ \t]*[0-9]*\. " || $i ~ "^:?[ \t]*((\* )|(\- )|(\+ ))") {
            header[i] = s_header
            footer[i] = s_footer
        } else {
            header[i] = ""
            footer[i] = ""
        }

    }

    # Leave only first header and last footer of consecutive Itemizations
    for (i = 2; i <= NF; i++) {
        if (header[i] == s_header) {
            j = i
            while (header[j] == s_header) {
                j += 1
            }

            # now only header[i] until footer[j-1] are neccessary and the rest may be deleted
            # if we need to delete at least one footer and one header
            if (j != i) {
                footer[i] = ""
                header[j-1] = ""
            }
            # if we need to delete more
            for (k = i + 1; k < j - 1; k++) {
                header[k] = ""
                footer[k] = ""
            }
        }
    }

    # Remove all colons at the beginning and remember to add another <br>
    for (i = 2; i <= NF; i++) {
        field[i] = $i
        newline_header[i] = ""

        if (field[i] ~ "^:[ \t]") {
            newline_header[i] = "<br>"
            sub(":", " ", field[i])
        }
        sub("  ", "", field[i])
    }
    newline_header[2] = ""

    back_page = ""
    for (i = 2; i <= NF; i++) {
        back_page = back_page newline_header[i] header[i] field[i] footer[i]
        if (i != NF) {
            back_page = back_page "<br>"
        }
    }

    # Replace markdown images by HTML-images
    while (match(back_page, "!\\[\\]\\((.*)\\)", a) > 0) {
        replace_str = "<img src=\"" a[1] "\">"
        before = substr(back_page, 1, RSTART - 1)
        after = substr(back_page, RSTART + RLENGTH)
        back_page = before replace_str after
    }

}

# Printing
/\n: / {
    # Apparently everything should be printed in one go. Every new print statement triggers the ORS to separate the data
    print front_page, back_page, "VL " lecture_number
}

# Update VL-Marker
/^#/ {
    if ($0 ~ "^\# VL ") {
        sub("\# VL ", "")
        split($0, vl_arr, ":")
        lecture_number = vl_arr[1]
    } else if ($0 ~ "^\# C") {
        sub("\# ", "")
        split($0, vl_arr, ":")
        lecture_number = vl_arr[1]
    }
}


END {
    #print "EOF"
}

# manual postprocessing might be necessary for full HTML compatibility. regexps:
# Replace 1. with 2.

# 1. ( |\n|\.|:|<|>|\(|\)|,)_(.*?)_( |\n|\.|:|<|>|\(|\)|,)
# 2. \1<em>\2</em>\3

# 1. ( |\n|\.|:|<|>|\(|\)|,|\t|\?|\!|\")\*\*(.*?)\*\*( |\n|\.|:|<|>|\(|\)|,|\t|\?|\!|\")
# 2. \1<b>\2</b>\3

# 1. ( |\n|\.|:|<|>|\(|\)|,|\w)~(.*?)~( |\n|\.|:|<|>|\(|\)|,|\w)
# 2. \1<sub>\2</sub>\3