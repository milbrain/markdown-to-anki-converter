#!/usr/bin/env python3
# Config files:
# configparser # https://docs.python.org/3/library/configparser.html
# shlex
# json
import subprocess as sp
import argparse
import sys

# Format soll sein:
# 1. Vorderseite \t 2. Rückseite (mittig) / Rückseite (Fließtext) \t 3. VL X \n
# 1. -> Titel des Definition-Blocks
# 2. -> Definitionsblock:
#       - Nur 1 Doppelpunkt: Simple-Antwort
#       - Mehr als 1 Doppelpunkt: Fließtext (class=explanation)
# 3. -> Die zuletzt begenete TL-Heading (#) wird bis zum Doppelpunkt geparst.
#    Dieser Abschnitt sollte [VL XX: Name der Vorlesung] heißen

tmp = '''
.explanation {
 text-align:left;
 width:max-content;
 max-width:100%;
 margin-left: auto;
 margin-right:auto;
}
'''


# def prepare(md_file):
#     f = open(md_file, "rb")
#     return


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output_file', help='File to output the ANKI notes to [.txt]', type=str)
    parser.add_argument('-i', '--input_file', help='File to parse', type=str)
    args = parser.parse_args()
    call_args = ['awk', '-f', 'processing.awk']

    if args.input_file:
        f_in = open(args.input_file, 'rb')
    else:
        f_in = sys.stdin.buffer
    text = f_in.read()

    if args.output_file:
        f_out = open(args.output_file, 'w+')
    else:
        f_out = sp.PIPE

    p = sp.run(call_args, input=text, stdout=f_out, stderr=sp.PIPE)
    if not args.output_file:
        print(p.stdout.decode(), end='')

    if p.returncode == 0:
        if args.output_file:
            f_out.close()
            print('Successfully read from {}'.format(args.input_file if args.input_file else 'stdin'))
            line_count = sum(1 for line in open(args.output_file))
            print('Written {} lines to:\t{}'.format(line_count, args.output_file))
    else:
        print(p.stderr)
        print('Failed.')
    f_in.close()

    exit(p.returncode)
